$(function() {
    'use strict';
    var detect_h = function() {
        var content = $('#page').innerHeight(),
            winH = $(window).height(),
            mainH = $('#main').height(),
            winW = $(window).width(),
            headerH = $('#header').height(),
            needH = winH - content;
        if (content < winH) {
            $('#main').css("min-height", mainH + needH);
        }
    };
    $(window).on('load', function() {
        detect_h();
    });
    $(window).resize(function() {
        detect_h();
    });
    // iPad and iPod detection  
    var isiPad = function() {
        return (navigator.platform.indexOf("iPad") != -1);
    };
    var isiPhone = function() {
        return (
            (navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPod") != -1)
        );
    };
    var windowScroll = function() {
        var winScroll = $(window).scrollTop() >= 1,
            test = $('#page').innerHeight(),
            winH = $(window).height(),
            headerH = $('#header').height(),
            x = test - winH;
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 1 && x >= headerH) {
                $('#header').addClass('fixed');
            }
            if ($(window).scrollTop() <= 0) {
                $('#header').removeClass('fixed');
            }
        });
    };
    // Burger Menu
    var burgerMenu = function() {
        var winW = $(window).width(),
            _text = '',
           _lang = $('#language').data('lang');//抓取當前語系
        //根據螢幕大小顯示對應的文字
        if (winW < 360) {
            if(_lang == 'tw'){
                _text = '繁';
            }
            else{
                _text = '简';
            }
        }
        else{
            if(_lang == 'tw'){
                _text = '繁體';
            }
            else{
                _text = '简体';
            }
        }
        $('#language .btn .lan').text(_text);
        $('#language .btn').click(function() {
            if(_lang == 'cn'){
                location.href='languages.php?lang=tw';
            }
            else{
                location.href='languages.php?lang=cn';
            }
        });
        $('.menu, .overlay').click(function() {
            $('.offcanvas-collapse').toggleClass('open');
            if ($('.offcanvas-collapse').hasClass('open')) {
                $('.burger').addClass('active');
                $('.overlay').fadeIn();
            } else {
                $('.burger').removeClass('active');
                $('.overlay').fadeOut();
            }
            $('body').toggleClass("no-scroll");
        });
    };
    var goToTop = function() {
        $('.js-gotop').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');
            return false;
        });
        $(window).scroll(function() {
            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }
        });
    };
    $(function() {
        windowScroll();
        burgerMenu();
        goToTop();
    });
});