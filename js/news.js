$(function() {
    var content = $('#page').innerHeight(),
        winH = $(window).height(),
        winW = $(window).width(),
        txt_content = $('.content').height(),
        needH = winH - content;
    var detect_h = function() {
        if (content < winH && winW > 767) {
            $('.content').css("min-height", txt_content + needH);
        }
    };
    detect_h();
    $('.ellipsis2').ellipsis({
        row: 2
    });
    $(window).resize(function() {
        detect_h();
    });
});