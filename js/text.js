$(function() {
    var content = $('#page').innerHeight(),
        winH = $(window).height(),
        winW = $(window).width(),
        txt_content = $('#about .text').innerHeight(),
        needH = winH - content;
    var detect_h = function() {
        if (content < winH && winW > 767) {
            $('#main').css("min-height", "auto");
            $('#about .text').css("min-height", txt_content + needH);
        }
    };
    detect_h();
    $(window).resize(function() {
        detect_h();
    });
});