$(function() {
    $('.gallery').each(function() {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            image: {
                markup: '<div class="mfp-figure">' +
                    '<div class="mfp-close"></div>' +
                    '<div class="mfp-img"></div>' +
                    '<div class="mfp-bottom-bar">' +
                    '<div class="mfp-title"></div>' +
                    '<div class="btn btn-sm btn-primary" title="產品詢價">加入詢價</div>' +
                    '<div class="mfp-counter"></div>' +
                    '</div>' +
                    '</div>',
                cursor: 'mfp-zoom-out-cur',
                titleSrc: 'title',
                verticalFit: true,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            gallery: {
                enabled: true
            }
        });
    });
    $('.gallery2').each(function() {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            image: {
                cursor: 'mfp-zoom-out-cur',
                titleSrc: 'title',
                verticalFit: true,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            gallery: {
                enabled: true
            }
        });
    });
    $("body").swipe({
        swipeLeft: function(event, direction, distance, duration, fingerCount) {
            $(".mfp-arrow-left").magnificPopup("prev");
        },
        swipeRight: function() {
            $(".mfp-arrow-right").magnificPopup("next");
        },
        threshold: 50
    });
});