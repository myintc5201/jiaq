$(function() {
    var winW = $(window).width();
    $('.index-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
    $('.pro-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerPadding: '40px',
        nextArrow: '<button class="slick-next"><span class="iconify" data-icon="simple-line-icons:arrow-right" data-inline="false"></span></button>',
        prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="simple-line-icons:arrow-left" data-inline="false"></span></button>',
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    if (winW > 767) {
        $('.ellipsis6').ellipsis({
            row: 6
        });
    };
    $('.ellipsis2').ellipsis({
        row: 2
    });
    $(window).resize(function() {
        if (winW > 767) {
            $('.ellipsis6').ellipsis({
                row: 6
            });
        };
    });
});